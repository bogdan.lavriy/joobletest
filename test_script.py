import argparse
import scalers
import sys

parser=argparse.ArgumentParser()

parser.add_argument('--train_file', help='Path to train file')
parser.add_argument('--test_file', help='Path to test file')

args=parser.parse_args()

zscaler = scalers.ZScaler()
zscaler.fit_transform(args.train_file, args.test_file)
