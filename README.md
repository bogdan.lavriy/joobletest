# JoobleTest

DS Test Task for Jooble

Run test script:

python3 ./test_script.py --train_file=/home/bogdan/Work/Jooble/python-dev-test/data/train.tsv --test_file=/home/bogdan/Work/Jooble/python-dev-test/data/test.tsv

I suspect that handling feature types is implemented not the way it was supposed. It would be easier to understand the task if it had at least some fake data of other types. If you would like to explain it in more detail I could modify the ZScaler class.
