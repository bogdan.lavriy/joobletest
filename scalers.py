import pandas as pd
import numpy as np
import os
import csv
from multiprocessing import cpu_count, Pool, JoinableQueue, Process, Manager

class ZScaler():
    
    def __init__(self, batch_size=10_000):
        self.batch_size = batch_size
        self.means = None
        self.stds = None
    
    def _means_worker(self, df):
        features = np.stack(df['features'].str.split(',').to_numpy(), axis=0).astype(int)
        batch_sum = features[:,1:].sum(axis=0)
        return batch_sum, features.shape[0]
        
    def _variations_worker(self, df):
        features = np.stack(df['features'].str.split(',').to_numpy(), axis=0).astype(int)
        batch_variations = np.power(features[:,1:] - self.means, 2).sum(axis=0)
        return batch_variations, features.shape[0]
    
    def _transform_worker(self, queue, df):
        job_ids = df['id_job']
        features = np.stack(df['features'].str.split(',').to_numpy(), axis=0).astype(float)
        features[:,1:] = (features[:,1:] - self.means) / self.stds
        maxes = np.amax(features[:,1:], axis=1)
        argmaxes = np.argmax(features[:,1:], axis=1)
        queue.put(zip(job_ids, features, maxes, argmaxes))
    
    def _write_worker(self, out_name, queue):
        with open(out_name, 'wt') as file:
            tsv_writer = csv.writer(file, delimiter='\t')
            tsv_writer.writerow(['id_job'] + \
                                [f'feature_2_stand_{i}' for i in range(256)] + \
                                ['max_feature_2_index', 'max_feature_2_abs_mean_diff'])
            while True:
                result = queue.get()
                for id_job, features, max_, argmax in result:
                    tsv_writer.writerow([id_job] + features[1:].tolist() + [max_, argmax])
                queue.task_done()
    
    def fit(self, train_name):
        # uses all cores, core i5-8250U
        # batch size 10K: 22.5 M rows / hour
        job_pool = Pool(cpu_count())
        results = job_pool.map(self._means_worker,
                               pd.read_csv(train_name,
                                           sep='\t',
                                           iterator=True,
                                           chunksize=self.batch_size))
        self.means = means = np.vstack([x[0] for x in results]).sum(axis=0) / sum([x[1] for x in results])
        
        results = job_pool.map(self._variations_worker,
                               pd.read_csv(train_name,
                                           sep='\t',
                                           iterator=True,
                                           chunksize=self.batch_size))
        job_pool.close()
        job_pool.join()
        variations = np.vstack([x[0] for x in results]).sum(axis=0) / (sum([x[1] for x in results]) - 1)
        self.stds = np.sqrt(variations)
        
    def transform(self, test_name):
        # uses all cores, core i5-8250U
        # batch size 10K: 4.9 M rows / hour
        
        out_name = test_name[:-4]+'_proc'+test_name[-4:]
        job_pool = Pool(cpu_count()-1)
        manager = Manager()
        queue = manager.JoinableQueue()
        writer = Process(target = self._write_worker,
                         args = (out_name, queue))
        writer.start()
        
        jobs = []
        for df in pd.read_csv(test_name,
                              sep='\t',
                              iterator=True,
                              chunksize=self.batch_size):
            job = job_pool.apply_async(self._transform_worker, (queue, df))
            jobs.append(job)

        job_pool.close()
        job_pool.join()
        queue.join()
    
    def fit_transform(self, train_name, test_name):
        self.fit(train_name)
        self.transform(test_name)

class SomeOtherScaler():
	pass
